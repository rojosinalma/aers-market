class Commodity
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  # field :name,          type: String
  # field :buyPrice,      type: Integer
  # field :sellPrice,     type: Integer
  # field :meanPrice,     type: Integer
  # field :stock,         type: Integer
  # field :stockBracket,  type: Integer
  # field :demand,        type: Integer
  # field :demandBracket, type: Integer
  # field :statusFlags,   type: Array
end
